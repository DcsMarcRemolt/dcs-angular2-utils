# @dcs/angular2-utils

Angular2 tools and infrastructure used at DCS Fürth.

This is a companion libary for our Angular2 starter project (currently not public).


## Table of Contents

- [Installation](#installation)
- [Features](#features)
- [Root Reducer](#root-reducer)


## Installation

In your Angular2 + TypeScript project:

```sh
npm install --save @dcs/angular2-utils immutable @angular2-redux/store redux-immutable redux-logger redux-observable
```

The other peer dependencies (@angular/core, rxjs ...) should already be there. It is an Angular2 project after all.

In the Root NgModule add Angular2UtilsModule to imports and inherit from MainBaseModule:

```typescript
import { NgModule } from '@angular/core';
import { Angular2UtilsModule, MainBaseModule } from '@dcs/angular2-utils';

@NgModule({
  /* ... */
  imports: [ /* ... */, Angular2UtilsModule ]
})
export class MainModule extends MainBaseModule { }
```

This does all the redux, DevTools, HMR ... wiring for the application.


## Features

* Pre-wired redux store via @angular2-redux/store
* Add reducers and epics via Angular DI
* Many ready to use validator functions
* Some basic reusable Components and Pipes
* RestService as a small configurable wrapper around Angular2 Http service
* Lots of small utility functions (see utils folder)


## Root Reducer

To add a new reducer to the Root Reducer, we use Angular DI.

Provide a new reducer for the token APP_REDUCERS, using { multi: true }:

```typescript
import { NgModule } from '@angular/core';
import { APP_REDUCERS } from '@dcs/angular2-utils';

import { myReducer } from './my.reducer';


@NgModule({
  providers: [
    { provide: APP_REDUCERS, useValue: { name: 'keyInState', reducer: myReducer } }
  ]
})
export class FeatureModule { }
```


## Routing via actions

Routing can be triggered via a Redux Action. Just inject RouterActions and dispatch the Action function routeChange.

The state below ['router', 'url'] also updates automatically when the route changes in app, even when not using the Action.


