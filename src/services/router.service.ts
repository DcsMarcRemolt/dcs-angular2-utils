import { Injectable } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { NgRedux, select } from '@angular-redux/store';
import { Observable } from 'rxjs/Observable';

import { RouterActions } from '../redux/router.actions';
import { IState } from '../interfaces';


@Injectable()
export class RouterService {

  @select(['router', 'url']) currentUrl: Observable<string>;

  constructor(router: Router, store: NgRedux<IState>, actions: RouterActions) {

    router.events
      .filter(event => event instanceof NavigationEnd)
      .distinctUntilChanged()
      .subscribe((event: NavigationEnd) => {
        if (event.url !== store.getState().getIn(['router', 'url'])) {
          store.dispatch(actions.routeChange(event.url));
        }
      });

    this.currentUrl
      .distinctUntilChanged()
      .subscribe((url: string) => {
        if (url.startsWith('http')) {
          // external link
          window.location.href = url;
          return;
        }

        if (router.url !== url) {
          router.navigate([url]);
        }
      });
  }

}
