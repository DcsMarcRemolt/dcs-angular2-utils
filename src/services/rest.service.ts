import { Injectable, Inject } from '@angular/core';
import { Http, Response, RequestOptions, RequestMethod } from '@angular/http';
import { Schema, normalize } from 'normalizr';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';

import { ISettings } from '../interfaces';
import { APP_SETTINGS } from '../tokens';

function extractResponseData(response: Response): any {
  let payload: any;

  try {
    payload = response.json();
  } catch (e) {
    payload = {};
  }

  return payload;
}

function normalizePayload(data: any, schema?: Schema) {
  if (schema) {
    return normalize(data, schema);
  }

  return data;
}

@Injectable()
export class RestService {
  constructor(
    protected http: Http,
    @Inject(APP_SETTINGS) protected settings: ISettings
  ) {}

  get(path: string, options: any = {}, schema?: Schema): Observable<any> {
    let requestOptions = new RequestOptions({
      method: RequestMethod.Get
    });

    return this.request(path, requestOptions.merge(options), schema);
  }

  post(
    path: string,
    body: any,
    options: any = {},
    schema?: Schema
  ): Observable<any> {
    let requestOptions = new RequestOptions({
      method: RequestMethod.Post,
      body: body
    });

    return this.request(path, requestOptions.merge(options), schema);
  }

  put(
    path: string,
    body: any,
    options: any = {},
    schema?: Schema
  ): Observable<any> {
    let requestOptions = new RequestOptions({
      method: RequestMethod.Put,
      body: body
    });

    return this.request(path, requestOptions.merge(options), schema);
  }

  delete(path: string, options: any = {}, schema?: Schema): Observable<any> {
    let requestOptions = new RequestOptions({
      method: RequestMethod.Delete
    });

    return this.request(path, requestOptions.merge(options), schema);
  }

  protected request(
    path: string,
    requestOptions: RequestOptions,
    schema?: Schema
  ): Observable<any> {
    let url: string;

    if (path.startsWith('http')) {
      url = path;
    } else {
      url = `${this.settings.apiUrl}/${path}`;
    }

    return this.http
      .request(url, requestOptions)
      .map(extractResponseData)
      .map(data => normalizePayload(data, schema))
      .share();
  }
}
