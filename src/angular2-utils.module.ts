import { NgModule, Inject, LOCALE_ID } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { TranslateService, TranslateModule } from '@ngx-translate/core';

import { APP_REDUCERS, APP_EPICS, APP_ERROR_FORMATTERS, APP_TRANSLATIONS } from './tokens';
import { RootReducer } from './redux/root.reducer';
import { RouterActions } from './redux/router.actions';
import { routerReducer } from './redux/router.reducer';
import { RootEpic, dummyEpic } from './redux/root.epic';

import { RestService } from './services/rest.service';
import { RouterService } from './services/router.service';


@NgModule({
  declarations: [],
  exports: [
    TranslateModule
  ],
  imports: [
    CommonModule,
    HttpModule,
    TranslateModule.forRoot(),
  ],
  providers: [
    RootReducer,
    RootEpic,
    RestService,
    RouterService,
    RouterActions,
    { provide: APP_REDUCERS, useValue: { name: 'router', reducer: routerReducer }, multi: true },
    { provide: APP_EPICS, useValue: dummyEpic, multi: true },
    { provide: APP_ERROR_FORMATTERS, useValue: {} },
    { provide: APP_TRANSLATIONS, useValue: { name: 'en', translations: {} }, multi: true }
  ]
})
export class Angular2UtilsModule {
  private routerService: RouterService;

  // RouterService needs to be instantiated at least once
  constructor(
    routerService: RouterService,
    private translate: TranslateService,
    @Inject(LOCALE_ID) private appLocale: string,
    @Inject(APP_TRANSLATIONS) private translations: Array<{ name: string, translations: any }>
  ) {
    this.routerService = routerService;
    this.setupTranslations();
  }

  setupTranslations() {
    this.translations.forEach((translation: { name: string, translations: any }) => {
      this.translate.setTranslation(translation.name, translation.translations || {}, true);
    });

    this.translate.setDefaultLang(this.appLocale);
    this.translate.use(this.appLocale);
  }

}



export function theTruth(): boolean {
  return true;
}
