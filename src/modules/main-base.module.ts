import { ApplicationRef, NgZone } from '@angular/core';
import { createNewHosts, createInputTransfer } from '@angularclass/hmr';
import { NgRedux } from '@angular-redux/store';
import { createEpicMiddleware } from 'redux-observable';
import { fromJS, Map } from 'immutable';

import { IState, ISubStateReducer } from '../interfaces';
import { RootReducer } from '../redux/root.reducer';
import { RootEpic } from '../redux/root.epic';
import { observableMiddleware, tickEnhancer } from '../utils/middleware';

export abstract class MainBaseModule {
  constructor(
    private appRef: ApplicationRef,
    private store: NgRedux<IState>,
    private devTools: any,
    private zone: NgZone,
    private rootReducer: RootReducer,
    private rootEpic: RootEpic
  ) {}

  hmrOnInit(hmrStore: any) {
    if (!hmrStore) return;

    if ('restoreInputValues' in hmrStore) {
      hmrStore.restoreInputValues();
    }
    // change detection
    this.appRef.tick();
    delete hmrStore.restoreInputValues;
    // console.timeEnd('hot reload');
  }

  hmrOnDestroy(hmrStore: any) {
    // store app state right before hot reload
    hmrStore.appState = this.store.getState();

    let cmpLocation = this.appRef.components.map(
      cmp => cmp.location.nativeElement
    );
    // recreate elements
    hmrStore.disposeOldHosts = createNewHosts(cmpLocation);

    hmrStore.restoreInputValues = createInputTransfer();
    // remove styles - don't use this with global styles (webpack style-loader)!
    // removeNgStyles();
  }

  hmrAfterDestroy(hmrStore: any) {
    // display new elements
    hmrStore.disposeOldHosts();
    delete hmrStore.disposeOldHosts;
    // console.time('hot reload');
  }

  protected setupStore(appState: IState = Map({})): void {
    let middleware = this.getMiddleware();
    let enhancers: Array<any> = [tickEnhancer(this.zone)];
    let tickTimeoutId: any;

    if (ENV === 'development') {
      // configure devtools if installed in Chrome
      if (this.devTools.isEnabled()) {
        enhancers = [
          ...enhancers,
          this.devTools.enhancer({
            deserializeState: (state: IState) => {
              // this is for loading the state via devtols from a JSON dump
              clearTimeout(tickTimeoutId);
              tickTimeoutId = setTimeout(() => {
                console.log('State restored, calling change detection.');
                // fire change detection
                this.zone.run(() => {});
              }, 100);
              return fromJS(state);
            }
          })
        ];
      }
    }

    this.store.configureStore(
      <ISubStateReducer<IState>>this.rootReducer.reducer(),
      appState,
      middleware,
      enhancers
    );
    // fire change detection
    this.zone.run(() => {});
  }

  protected getMiddleware(): Array<any> {
    return [observableMiddleware, createEpicMiddleware(this.rootEpic.epic())];
  }
}
