import { TranslateService } from '@ngx-translate/core';

import { RootReducer } from '../redux/root.reducer';
import { NgRedux } from '@angular-redux/store';
import { IState, ISubStateReducer } from '../interfaces';


export abstract class DynamicModule {

  constructor(
    private translate: TranslateService,
    private translations: Array<{ name: string, translations: any }>,
    rootReducer: RootReducer,
    store: NgRedux<IState>,
    appReducers: any
  ) {
    store.replaceReducer(<ISubStateReducer<IState>>rootReducer.reducer(appReducers));
    this.setupTranslations();
  }

  setupTranslations() {
    this.translations.forEach((translation: { name: string, translations: any }) => {
      this.translate.setTranslation(translation.name, translation.translations, true);
    });
  }
}
