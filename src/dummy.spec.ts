import { expect } from 'chai';

import { theTruth } from './angular2-utils.module';


describe('the test suite', () => {

  it('runs', () => {
    expect(42).to.equal(42);
    expect(42).to.not.equal(43);
  });

  it('imports files', () => {
    expect(theTruth()).to.be.true;
  });

});
