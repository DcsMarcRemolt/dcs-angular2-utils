import { expect } from 'chai';
import { OutputSelector } from 'reselect';

import { createSelector } from './reselect';
import { IState } from '../interfaces';
import { fromJS, Map, List } from 'immutable';

describe('createSelector', () => {
  let subject: OutputSelector<any, any, any>;
  // let subject: string;

  let state: IState;

  let counter: number;

  beforeEach(() => {
    counter = 0;

    subject = createSelector(
      [(state: IState): number => state.get('foo'), (state: IState): number => state.get('bar')],
      (foo: number, bar: number) => {
        counter += 1;
        return foo + bar;
      }
    );

    state = fromJS({
      foo: 12,
      bar: 42
    });
  });

  it('returns a function', () => {
    expect(subject).to.be.a('function');
  });

  it('correctly calls the embedded function', () => {
    expect(subject(state)).to.equal(54);
  });

  it('only calls the function once with same state', () => {
    expect(subject(state)).to.equal(54);
    subject(state);
    subject(state);

    expect(counter).to.equal(1);
  });

  it('recalls the function with different state', () => {
    let newState = state.set('foo', 13);

    expect(subject(state)).to.equal(54);
    subject(state);
    subject(state);
    expect(counter).to.equal(1);

    expect(subject(newState)).to.equal(55);
    expect(counter).to.equal(2);
    subject(newState);
    expect(counter).to.equal(2);

    expect(subject(state)).to.equal(54);

    expect(counter).to.equal(3);
  });

  describe('with deep immutable data', () => {
    beforeEach(() => {
      counter = 0;

      subject = createSelector(
        [(state: IState): Map<string, any> => state.get('foo'), (state: IState): List<number> => state.get('bar')],
        (foo: Map<string, any>, bar: List<number>) => {
          counter += 1;

          let name = `${foo.getIn(['name', 'first'])} ${foo.getIn(['name', 'last'])}`;
          let age = bar.reduce((sum: number, num: number) => sum + num);
          return `${name} (${age})`;
        }
      );

      state = fromJS({
        foo: { hello: 'world', name: { first: 'Arthur', last: 'Dent' } },
        bar: [1, 2, 3, 4, 5]
      });
    });

    it('correctly calls the embedded function', () => {
      expect(subject(state)).to.equal('Arthur Dent (15)');
    });

    it('only calls the function once with same state', () => {
      subject(state);
      subject(state);
      expect(subject(state)).to.equal('Arthur Dent (15)');

      expect(counter).to.equal(1);
    });

    it('recalls the function with different state', () => {
      let newState = state.update('bar', bar => bar.push(27));

      expect(subject(state)).to.equal('Arthur Dent (15)');
      subject(state);
      subject(state);
      expect(counter).to.equal(1);

      expect(subject(newState)).to.equal('Arthur Dent (42)');
      expect(counter).to.equal(2);
      subject(newState);
      expect(counter).to.equal(2);

      expect(subject(state)).to.equal('Arthur Dent (15)');

      expect(counter).to.equal(3);
    });
  });
});
