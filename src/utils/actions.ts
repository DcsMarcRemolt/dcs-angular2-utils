
export function generateAsyncActionNames(baseName: string): Array<string> {
  return [
    baseName,
    baseName + '_START',
    baseName + '_NEXT',
    baseName + '_ERROR',
  ];
}
