import { createSelectorCreator, defaultMemoize, createSelector as cs } from 'reselect';
import { is } from 'immutable';

export const createSelector: typeof cs = createSelectorCreator(defaultMemoize, is);
