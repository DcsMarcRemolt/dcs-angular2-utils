import { expect } from 'chai';
import { FormControl } from '@angular/forms';

import { validateEmail, validatePhone, validatePositiveInteger } from './validators';


describe('validators', () => {

  describe('validateEmail', () => {
    it('returns null when given a valid address', () => {
      let control: FormControl = <FormControl>{ value: 'test@example.com' };
      expect(validateEmail(control)).to.be.null;
    });

    it('returns the correct error when given an invalid address', () => {
      let control: FormControl = <FormControl>{ value: 'test@example' };
      expect(validateEmail(control)).to.eql({ validateEmail: { valid: false } });
    });
  });

  describe('validatePhone', () => {
    it('returns null when given a valid value', () => {
      let control: FormControl = <FormControl>{ value: '+49 123 456 789' };
      expect(validatePhone(control)).to.be.null;
    });

    it('returns the correct error when given an invalid value', () => {
      let control: FormControl = <FormControl>{ value: '1234 abc' };
      expect(validatePhone(control)).to.eql({ validatePhone: { valid: false } });
    });
  });

  describe('validatePositiveInteger', () => {

    it('returns null if given a number string > 0', () => {
      let control = <FormControl>{ value: '12' };
      expect(validatePositiveInteger(control)).to.eql(null);
    });

    it('returns error object if given a number string == 0', () => {
      let control = <FormControl>{ value: '0' };
      expect(validatePositiveInteger(control)).to.eql({ validatePositiveInteger: { valid: false } });
    });

    it('returns error object if given a number string < 0', () => {
      let control = <FormControl>{ value: '-1' };
      expect(validatePositiveInteger(control)).to.eql({ validatePositiveInteger: { valid: false } });
    });

    it('returns error object if given a string', () => {
      let control = <FormControl>{ value: 'ARGH' };
      expect(validatePositiveInteger(control)).to.eql({ validatePositiveInteger: { valid: false } });
    });

  });

});
