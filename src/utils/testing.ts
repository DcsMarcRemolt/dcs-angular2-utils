import { NgRedux } from '@angular-redux/store';
import configureStore from 'redux-mock-store';
import { createEpicMiddleware } from 'redux-observable';
import { fromJS } from 'immutable';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { observableMiddleware } from './middleware';
import { IState, ISubStateReducer } from '../interfaces';
import { RootReducer } from '../redux/root.reducer';
import { RootEpic } from '../redux/root.epic';


const middlewares = [observableMiddleware];
export const createMockStore: Function = configureStore(middlewares);

export function setupNgRedux(ngRedux: NgRedux<IState>, rootReducer: RootReducer, rootEpic: RootEpic, initialState: IState = fromJS({})): NgRedux<IState> {
  let middleware: Array<any> = [
    observableMiddleware,
    createEpicMiddleware(rootEpic.epic())
  ];

  ngRedux.configureStore(<ISubStateReducer<IState>>rootReducer.reducer(), initialState, middleware);
  return ngRedux;
}

export class FakeRestService {

  public path: string;
  public options: any;
  public body: any;

  get(path: string, options: any = {}): Observable<any> {
    this.path = path;
    this.options = options;
    return Observable.of('fake payload');
  }

  post(path: string, body: any, options: any = {}): Observable<any> {
    this.path = path;
    this.options = options;
    this.body = body;
    return Observable.of('fake payload');
  }

  put(path: string, body: any, options: any = {}): Observable<any> {
    this.path = path;
    this.body = body;
    this.options = options;
    return Observable.of('fake payload');
  }

  delete(path: string, options: any = {}): Observable<any> {
    this.path = path;
    this.options = options;
    return Observable.of('fake payload');
  }

}
