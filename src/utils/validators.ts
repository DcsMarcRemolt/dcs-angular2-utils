import { FormControl } from '@angular/forms';
import * as validator from 'validator';
import * as moment from 'moment';

export function validateEmail(c: FormControl) {
  return validator.isEmail(String(c.value)) ? null : { validateEmail: { valid: false } };
}

export function validatePhone(c: FormControl) {
  return validator.isMobilePhone(String(c.value), 'de-DE') ? null : { validatePhone: { valid: false } };
}

export function validateFQDN(c: FormControl) {
  return validator.isFQDN(String(c.value)) ? null : { validateFQDN: { valid: false } };
}

export function validatePositiveInteger(c: FormControl) {
  let value: string = String(c.value);
  return validator.isInt(value, { min: 1 }) ? null : { validatePositiveInteger: { valid: false } };
}

export function validateDate(c: FormControl) {
  let value: moment.Moment = moment(c.value, 'YYYY-MM-DD');
  return value.isValid() ? null : { validateDate: { valid: false } };
}

export function validateTime(c: FormControl) {
  let value: moment.Moment = moment(c.value, 'HH:mm');
  return value.isValid() ? null : { validateTime: { valid: false } };
}
