import { fromJS } from 'immutable';

import { IAction, IReducer, IState, ISubState } from '../interfaces';

export function createReducer<T extends ISubState>(
  initialState: T,
  handlers: { [key: string]: IReducer }
) {
  return function reducer(state: T = initialState, action?: IAction): T {
    if (action && handlers.hasOwnProperty(action.type)) {
      return <T>handlers[action.type](state, action);
    } else {
      return state;
    }
  };
}

export function mergeState(state: IState, newState: any): IState {
  return state.merge(fromJS(newState));
}
