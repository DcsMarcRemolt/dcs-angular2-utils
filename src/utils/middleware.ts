import { NgZone } from '@angular/core';
import { Response } from '@angular/http';
import { NgRedux } from '@angular-redux/store';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/takeUntil';

import { IState, IAction, IReducer } from '../interfaces';

// Middleware to automatically dispatch async actions when getting an Observable as payload
export const observableMiddleware: any = (store: NgRedux<IState>) => (
  next: Function
) => (action: IAction) => {
  if (action.payload instanceof Observable) {
    let baseType: string = action.type;
    let obs: Observable<any> = action.payload;

    if (action.meta && action.meta.cancel) {
      // if the action has a cancel observable, use it
      obs = obs.takeUntil(action.meta.cancel);
    }

    store.dispatch({
      type: `${baseType}_START`,
      meta: action.meta
    });

    obs.subscribe(
      (data: any) =>
        store.dispatch({ type: `${baseType}_NEXT`, payload: data }),
      (error: Response) =>
        store.dispatch({ type: `${baseType}_ERROR`, payload: error }),
      () => {
        if (action.meta && action.meta.dispatchCompleted) {
          store.dispatch({ type: `${baseType}_COMPLETED` });
        }
      }
    );
  } else {
    return next(action);
  }
};

/**
 * Store enhancer to ensure that after a state change there always is a angular2 tick to ensure change detection
 *
 * @param {NgZone} zone
 * @returns any
 *
 * TODO: test if this is still necessary or NgRedux now handles this correct
 */
export function tickEnhancer(zone: NgZone) {
  return (next: Function) => (
    reducer: IReducer,
    initialState: IState,
    enhancer: any
  ) => {
    let store = next(reducer, initialState, enhancer);

    store.subscribe(() => {
      // fire change detection
      zone.run(() => {});
    });

    return store;
  };
}
