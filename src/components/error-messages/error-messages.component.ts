import { Component, Input, Inject } from '@angular/core';
import { APP_ERROR_FORMATTERS } from '../../tokens';

const ERROR_FORMATTERS: {
  [key: string]: (error: { [key: string]: any }) => string;
} = {
  required: () => 'Pflichtfeld.',
  minlength: (error: { [key: string]: any }) =>
    `Eingabe zu kurz, es müssen mindestens ${error.requiredLength} Zeichen eingegeben werden.`,
  maxlength: (error: { [key: string]: any }) =>
    `Eingabe zu lang, es dürfen maximal ${error.requiredLength} Zeichen eingegeben werden.`,
  validateEmail: () => `Eingabe ist keine gültige E-Mail Adresse.`,
  validatePhone: () => `Eingabe ist keine gültige Telefonnummer.`,
  validatePositiveInteger: () => `Eingabe ist keine positive Zahl.`,
  validateDate: () => `Eingabe ist kein gültiges Datum.`,
  validateTime: () => `Eingabe ist keine gültige Uhrzeit.`,
  validateTheAnswer: () => `DIE Antwort ist leider nicht korrekt`
};

@Component({
  selector: 'dcs-error-messages',
  template: `
    <span class="text-danger">{{ renderErrors() }}</span>
  `
})
export class ErrorMessagesComponent {
  @Input() errors: { [key: string]: any } = {};

  get messageFormatters(): { [key: string]: Function } {
    return Object.assign({}, ERROR_FORMATTERS, this.errorFormatters);
  }

  constructor(@Inject(APP_ERROR_FORMATTERS) private errorFormatters: any) {}

  renderErrors(): string {
    let messages: any = Object.keys(this.errors).map(
      this.renderError.bind(this)
    );

    if (messages.length > 0) {
      return messages.join(' ');
    } else {
      return '';
    }
  }

  renderError(errorKey: string): string {
    if (this.messageFormatters[errorKey]) {
      return this.messageFormatters[errorKey](this.errors[errorKey]);
    } else {
      return `Error formatter '${errorKey}' missing!`;
    }
  }
}
