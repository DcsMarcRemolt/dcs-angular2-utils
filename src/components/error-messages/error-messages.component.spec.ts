import { expect } from 'chai';

import { ErrorMessagesComponent } from './error-messages.component';


describe('ErrorMessagesComponent', () => {

  let subject: ErrorMessagesComponent;

  beforeEach(() => {
    subject = new ErrorMessagesComponent({});
    subject.errors = {
      required: false,
      maxlength: { requiredLength: 42 },
      validatePositiveInteger: false,
      notExisting: false
    };
  });

  describe('renderError', () => {

    it('renders the required error', () => {
      expect(subject.renderError('required')).to.equal('Pflichtfeld.');
    });

    it('renders the maxLength error', () => {
      expect(subject.renderError('maxlength')).to.equal('Eingabe zu lang, es dürfen maximal 42 Zeichen eingegeben werden.');
    });

    it('renders the validatePositiveInteger error', () => {
      expect(subject.renderError('validatePositiveInteger')).to.equal('Eingabe ist keine positive Zahl.');
    });

    it('renders the default message if no formatter is available', () => {
      expect(subject.renderError('notExisting')).to.equal('Error formatter \'notExisting\' missing!');
    });

  });


  describe('renderErrors', () => {

    it('renders all erros a one string', () => {
      expect(subject.renderErrors())
        .to.equal('Pflichtfeld. Eingabe zu lang, es dürfen maximal 42 Zeichen eingegeben werden. Eingabe ist keine positive Zahl. Error formatter \'notExisting\' missing!');
    });

  });

  describe('overwriting default messages', () => {

    beforeEach(() => {
      subject = new ErrorMessagesComponent({
        validatePositiveInteger: () => 'New error message'
      });
      subject.errors = {
        required: false,
        maxlength: { requiredLength: 42 },
        validatePositiveInteger: false,
        notExisting: false
      };
    });

    it('renders the overwritten error message', () => {
      expect(subject.renderError('validatePositiveInteger')).to.equal('New error message');
    });

  });

});
