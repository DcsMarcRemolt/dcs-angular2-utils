import { FormGroup, FormControl, Validators } from '@angular/forms';
import { expect } from 'chai';
import { fromJS } from 'immutable';

import { PresentationalComponent } from './presentational-component';


describe('PresentationalComponent', () => {

  let subject: PresentationalComponent;

  beforeEach(() => {
    subject = new PresentationalComponent();
  });


  describe('#identify', () => {

    it('returns the hashCode for the given Object', () => {
      const obj = fromJS({
        test: 'object'
      });
      expect(subject.identify(0, obj)).to.equal(obj.hashCode());
    });

    it('throws TypeError if not given an Immutable', () => {
      const obj: any = {
        test: 'object'
      };
      expect(() => subject.identify(0, obj)).to.throw(TypeError, 'Given item is not of type Immutable, [object Object] given!');

    });

  });


  describe('#hasError', () => {

    let form: FormGroup;

    beforeEach(() => {
      form = new FormGroup({
        firstName: new FormControl(''),
        lastName: new FormControl('', Validators.required)
      });
    });


    describe('given a pristine form', () => {

      it('always returns false', () => {
        expect(subject.hasError(form, 'firstName')).to.be.false;
        expect(subject.hasError(form, 'lastName')).to.be.false;
      });

    });


    describe('given a touched form', () => {

      beforeEach(() => {
        form.get('firstName').markAsTouched();
        form.get('lastName').markAsTouched();
      });

      describe('if given an errorName', () => {

        it('returns true if the control has the specific error', () => {
          expect(subject.hasError(form, 'lastName', 'required')).to.be.true;
        });

        it('returns false if the control does not have the specific error', () => {
          expect(subject.hasError(form, 'lastName', 'foobar')).to.be.false;
        });

        it('returns false if the control has no errors', () => {
          expect(subject.hasError(form, 'firstName')).to.be.false;
        });

      });

      describe('if given no errorName', () => {

        it('returns true if the control has errors', () => {
          expect(subject.hasError(form, 'lastName')).to.be.true;
        });

        it('returns false if the control has no errors', () => {
          expect(subject.hasError(form, 'firstName')).to.be.false;
        });

      });

    });

  });

});
