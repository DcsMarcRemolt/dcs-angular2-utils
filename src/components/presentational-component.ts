import { FormGroup } from '@angular/forms';
import { Map } from 'immutable';

/**
 * Base class for *dumb* components
 *
 * Data is transfered via Inputs and Outputs, no Store or Actions allowed.
 *
 * @export
 * @class PresentationalComponent
 */
export class PresentationalComponent {

  /**
   * ImmutableJS aware track function
   *
   * Mainly used in ngFor trackBy
   *
   * @param {number} index
   * @param {Map<string, any>} item
   * @returns {number}
   *
   * @memberOf PresentationalComponent
   */
  identify(_: number, item: Map<string, any>): number {
    if (typeof item.hashCode !== 'function') {
      throw new TypeError(`Given item is not of type Immutable, ${item.toString()} given!`);
    }
    return item.hashCode();
  }

   /**
    * Tests, wether a form component has errors
    *
    * @param {FormGroup} form
    * @param {string} fieldName
    * @param {string} [errorName]
    * @returns {boolean}
    *
    * @memberOf PresentationalComponent
    */
   hasError(form: FormGroup, fieldName: string, errorName?: string): boolean {
     if (form.get(fieldName).untouched) {
       return false;
     }

    if (errorName) {
      return !!(form.get(fieldName).errors && form.get(fieldName).errors[errorName]);
    } else {
      return !!form.get(fieldName).errors;
    }
  }

}
