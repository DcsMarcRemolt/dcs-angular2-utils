import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';


@Component({
  selector: 'dcs-form-group',
  template: `
    <div class="form-group row" [ngClass]="{ 'has-error': invalid }">
      <label class="col-sm-{{ labelWidth }} control-label" [attr.for]="inputId">{{ label }}</label>
      <div class="col-sm-{{ 12 - labelWidth }}">
        <ng-content></ng-content>
        <dcs-error-messages *ngIf="invalid" [errors]="control.errors"></dcs-error-messages>
      </div>
    </div>
  `
})
export class FormGroupComponent {
  @Input() control: FormControl;
  @Input() label: string;
  @Input() inputId: string;
  @Input() labelWidth: number = 3;

  get invalid(): boolean {
    return this.control.invalid && this.control.dirty && this.control.touched;
  }

}
