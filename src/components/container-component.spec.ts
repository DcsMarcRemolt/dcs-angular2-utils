import { expect } from 'chai';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';

import { ContainerComponent } from './container-component';


describe('ContainerComponent', () => {

  let subject: ContainerComponent;
  let observable: Subject<number>;

  beforeEach(() => {
    subject = new ContainerComponent();
    observable = new Subject();
  });

  afterEach(() => {
    subject.subscriptions.forEach(sub => sub.unsubscribe());
  });


  describe('#subscribeToObservable', () => {

    it('adds the subscription to #subscriptions', () => {
      expect(subject.subscriptions.length).to.equal(0);
      subject.subscribeToObservable(observable, () => { });
      expect(subject.subscriptions.length).to.equal(1);
      expect(subject.subscriptions[0]).to.be.instanceof(Subscription);
    });

    it('runs the callback function', (done) => {
      const val: number = 42;
      const cb = function(value: number) {
        expect(value).to.equal(val);
        done();
      };

      subject.subscribeToObservable(observable, cb);
      observable.next(val);
    });

  });


  describe('#valueFromObservable', () => {

    it('adds the subscription to #subscriptions', () => {
      expect(subject.subscriptions.length).to.equal(0);
      subject.valueFromObservable(observable, 'foobar');
      expect(subject.subscriptions.length).to.equal(1);
      expect(subject.subscriptions[0]).to.be.instanceof(Subscription);
    });

    it('sets the value', () => {
      const val: number = 42;
      subject.valueFromObservable(observable, 'foobar');
      observable.next(val);
      expect(subject['foobar']).to.equal(val);
    });

  });

  describe('#ngOnDestroy', () => {

    beforeEach(() => {
      subject.subscriptions.push(observable.subscribe(() => {}));
      subject.subscriptions.push(observable.subscribe(() => {}));
    });

    it('unsubscribes from all subscriptions', () => {
      let subscriptions = subject.subscriptions;

      expect(subscriptions.length).to.equal(2);
      expect(subscriptions.every(s => !s.closed)).to.be.true;

      subject.ngOnDestroy();

      expect(subscriptions.every(s => !s.closed)).to.be.false;
      expect(subject.subscriptions.length).to.equal(0);
    });

  });

});
