import { OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';


export class ContainerComponent implements OnDestroy {

  subscriptions: Array<Subscription> = [];
  [key: string]: any;

  ngOnDestroy() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });

    this.subscriptions = [];
  }

  subscribeToObservable(obs: Observable<any>, callback: Function) {
    this.subscriptions.push(obs.subscribe(callback.bind(this)));
  }

  valueFromObservable<T>(obs: Observable<T>, propertyName: string) {
    this.subscribeToObservable(obs, (data: T) => {
      if (data) {
        this[propertyName] = data;
      }
    });
  }
}
