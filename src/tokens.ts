import { OpaqueToken } from '@angular/core';

export const APP_SETTINGS: OpaqueToken = new OpaqueToken('APP_SETTINGS');
export const APP_REDUCERS: OpaqueToken = new OpaqueToken('APP_REDUCERS');
export const APP_EPICS: OpaqueToken = new OpaqueToken('APP_EPICS');
export const APP_ERROR_FORMATTERS: OpaqueToken = new OpaqueToken('APP_ERROR_FORMATTERS');
export const APP_TRANSLATIONS: OpaqueToken = new OpaqueToken('APP_TRANSLATIONS');
