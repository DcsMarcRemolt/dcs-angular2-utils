declare var ENV: string;
declare var HMR: boolean;

declare var chaiImmutable: any;

declare module 'chai-immutable' {
  export = chaiImmutable;
}
