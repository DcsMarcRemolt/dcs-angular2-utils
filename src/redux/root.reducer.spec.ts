import { RootReducer } from './root.reducer';
import { IState, IAction, IReducer } from '../interfaces';
import { expect } from 'chai';

function dummyReducer(state: IState, action: IAction): IState {
  switch (action.type) { }

  return state;
}

class ClassReducer {
  reducer(): IReducer {
    return dummyReducer;
  }
}

describe('RootReducer', () => {

  let subject: RootReducer;
  let reducerConfigs = [
    { name: 'fooReducer', reducer: dummyReducer },
    { name: 'classReducer', reducer: new ClassReducer }
  ];

  beforeEach(() => {
    subject = new RootReducer(reducerConfigs);
  });

  it('adds the reducer function to the combined reducers', () => {
    let reducers = subject.buildReducers();
    expect(reducers.fooReducer).to.equal(dummyReducer);
  });

  it('adds the return value of ClassReducer#reducer() to the combined reducers', () => {
    let reducers = subject.buildReducers();
    expect(reducers.classReducer).to.equal(dummyReducer);
  });

});
