import * as chai from 'chai';
import * as chaiImmutable from 'chai-immutable';
import { expect } from 'chai';

import { routerReducer, initialState } from './router.reducer';
import { IState } from '../interfaces';
import { ROUTE_CHANGE } from './router.actions';

chai.use(chaiImmutable);


describe('routeReducer', () => {

  it('loads the correct initial state', () => {
    const nextState: IState = routerReducer(undefined, { type: '@@INIT' });
    expect(nextState).to.equal(initialState);
  });

  it('sets the correct url', () => {
    const nextState: IState = routerReducer(initialState, { type: ROUTE_CHANGE, payload: '/foo' });
    const expectedState: IState = initialState.set('url', '/foo');
    expect(nextState).to.equal(expectedState);
  });

});
