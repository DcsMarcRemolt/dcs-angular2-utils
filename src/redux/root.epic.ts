import { Injectable, Inject } from '@angular/core';
import { combineEpics, Epic, ActionsObservable } from 'redux-observable';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/empty';
import 'rxjs/add/operator/mergeMap';

import { IAction, IState } from '../interfaces';
import { APP_EPICS } from '../tokens';

// create one noop epic to fill provider with at least one value
// for epics docs see https://redux-observable.js.org/
export function dummyEpic(action$: ActionsObservable<IAction>) {
  return action$.mergeMap(() => {
    return Observable.empty();
  });
}

@Injectable()
export class RootEpic {
  constructor(
    @Inject(APP_EPICS) private epicConfigs: Array<Epic<any, IState>>
  ) {}

  epic(): any {
    return combineEpics(...this.epicConfigs);
  }
}
