import { Injectable } from '@angular/core';
import { IAction } from '../interfaces';


export const ROUTE_CHANGE: string = 'ROUTE_CHANGE';

@Injectable()
export class RouterActions {

  routeChange(url: string): IAction {
    return {
      type: ROUTE_CHANGE,
      payload: url
    };
  }

}
