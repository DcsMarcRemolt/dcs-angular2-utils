import { expect } from 'chai';

import { RouterActions, ROUTE_CHANGE } from './router.actions';


describe('RouterActions', () => {

  let subject: RouterActions;

  beforeEach(() => {
    subject = new RouterActions();
  });

  describe('routeChange', () => {

    it('returns the correct action', () => {
      expect(subject.routeChange('/test')).to.eql({
        type: ROUTE_CHANGE,
        payload: '/test'
      });
    });

  });

});
