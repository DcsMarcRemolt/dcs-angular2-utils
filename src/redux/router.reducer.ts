import { fromJS } from 'immutable';

import { IState, IAction } from '../interfaces';
import { ROUTE_CHANGE } from './router.actions';


export const initialState: IState = fromJS({
  url: '/'
});


export function routerReducer(state: IState = initialState, action: IAction): IState {
  switch (action.type) {

    case ROUTE_CHANGE:
      return state.set('url', action.payload);

  }

  return state;
}
