import { Injectable, Inject } from '@angular/core';
import { combineReducers } from 'redux-immutable';

import { IReducer } from '../interfaces';
import { APP_REDUCERS } from '../tokens';


@Injectable()
export class RootReducer {

  currentReducers: any = {};

  constructor(@Inject(APP_REDUCERS) private reducerConfigs: Array<any>) { }

  reducer(newReducerConfigs?: Array<any>): IReducer {
    return <IReducer>combineReducers(this.buildReducers(newReducerConfigs));
  }

  buildReducers(newReducerConfigs?: Array<any>) {
    let currentReducers = Object.assign({}, this.currentReducers);
    let reducerConfigs: Array<any> = newReducerConfigs ? newReducerConfigs : this.reducerConfigs;

    this.currentReducers = reducerConfigs
      .reduce((r, config) => {
        if (typeof config.reducer.reducer === 'function') {
          r[config.name] = config.reducer.reducer();
        } else {
          r[config.name] = config.reducer;
        }

        return r;
      }, currentReducers);

    return this.currentReducers;
  }

}
