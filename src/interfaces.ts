import { Observable } from 'rxjs/Observable';
import { Map, List, OrderedMap, Record, Set, OrderedSet } from 'immutable';

export interface ISettings {
  apiUrl: string;
}

export interface IState extends Map<string, any> {}

export type IImmutableObject =
  | Map<string, any>
  | Record.Class<any>
  | OrderedMap<string, any>;
export type IImmutableCollection =
  | List<IImmutableObject>
  | Set<IImmutableObject>
  | OrderedSet<IImmutableObject>;
export type ISubState =
  | IState
  | List<any>
  | Record.Class<any>
  | number
  | string
  | boolean;

export interface IAction {
  type: string;
  payload?: any;
  meta?: {
    dispatchCompleted?: boolean;
    cancel?: Observable<any>;
    searchTerm?: 'string';
  };
}

export interface ISubStateReducer<T extends ISubState> {
  (state?: T, action?: IAction): T;
}

export interface IReducer extends ISubStateReducer<ISubState> {}
