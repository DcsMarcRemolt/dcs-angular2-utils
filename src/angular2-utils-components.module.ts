import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';

import { FormGroupComponent } from './components/form-group/form-group.component';
import { ErrorMessagesComponent } from './components/error-messages/error-messages.component';

import { PricePipe } from './pipes/price-pipe/price-pipe';


@NgModule({
  declarations: [
    FormGroupComponent,
    ErrorMessagesComponent,
    PricePipe,
  ],
  exports: [
    FormGroupComponent,
    ErrorMessagesComponent,
    PricePipe
  ],
  imports: [
    CommonModule,
    HttpModule
  ],
  providers: []

})
export class Angular2UtilsComponentsModule { }
