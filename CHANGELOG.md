<a name="0.5.0"></a>
# 0.5.0 (2017-01-02)


### Bug Fixes

* Fixed ErrorMessagesComponent ([2f040b1](https://bitbucket.org/DcsMarcRemolt/dcs-angular2-utils/commits/2f040b1))
* Reverted build, no .ts files in dist ([08384f2](https://bitbucket.org/DcsMarcRemolt/dcs-angular2-utils/commits/08384f2))

### Features

* Added conventional-changelog-lint ([5e2514d](https://bitbucket.org/DcsMarcRemolt/dcs-angular2-utils/commits/5e2514d))
* Added ErrorMessagesComponent, added specs ([c0c0b95](https://bitbucket.org/DcsMarcRemolt/dcs-angular2-utils/commits/c0c0b95))
* Added price pipe from starter project, made error-messages-component configurabl ([c76f72c](https://bitbucket.org/DcsMarcRemolt/dcs-angular2-utils/commits/c76f72c))
* RootReducer can now be used inside lazy loaded modules. ([b324972](https://bitbucket.org/DcsMarcRemolt/dcs-angular2-utils/commits/b324972))
* RootReducer now accepts Reducer classes ([d1bc9a0](https://bitbucket.org/DcsMarcRemolt/dcs-angular2-utils/commits/d1bc9a0))



<a name="0.1.0"></a>
# 0.1.0 (2016-12-12)


### Bug Fixes

* Fixed ErrorMessagesComponent ([2f040b1](https://bitbucket.org/DcsMarcRemolt/dcs-angular2-utils/commits/2f040b1))
* Reverted build, no .ts files in dist ([08384f2](https://bitbucket.org/DcsMarcRemolt/dcs-angular2-utils/commits/08384f2))


### Features

* Added conventional-changelog-lint ([5e2514d](https://bitbucket.org/DcsMarcRemolt/dcs-angular2-utils/commits/5e2514d))
* Added ErrorMessagesComponent, added specs ([c0c0b95](https://bitbucket.org/DcsMarcRemolt/dcs-angular2-utils/commits/c0c0b95))
* Added price pipe from starter project, made error-messages-component configurable ([c76f72c](https://bitbucket.org/DcsMarcRemolt/dcs-angular2-utils/commits/c76f72c))



<a name="0.0.8"></a>
## 0.0.8 (2016-12-12)


### Bug Fixes

* Fixed ErrorMessagesComponent ([2f040b1](https://bitbucket.org/DcsMarcRemolt/dcs-angular2-utils/commits/2f040b1))
* Reverted build, no .ts files in dist ([08384f2](https://bitbucket.org/DcsMarcRemolt/dcs-angular2-utils/commits/08384f2))


### Features

* Added conventional-changelog-lint ([5e2514d](https://bitbucket.org/DcsMarcRemolt/dcs-angular2-utils/commits/5e2514d))
* Added ErrorMessagesComponent, added specs ([c0c0b95](https://bitbucket.org/DcsMarcRemolt/dcs-angular2-utils/commits/c0c0b95))
* Added price pipe from starter project, made error-messages-component configurable ([c76f72c](https://bitbucket.org/DcsMarcRemolt/dcs-angular2-utils/commits/c76f72c))



<a name="0.0.3"></a>
## 0.0.3 (2016-12-09)


### Bug Fixes

* Reverted build, no .ts files in dist ([08384f2](https://bitbucket.org/DcsMarcRemolt/dcs-angular2-utils/commits/08384f2))


### Features

* Added conventional-changelog-lint ([5e2514d](https://bitbucket.org/DcsMarcRemolt/dcs-angular2-utils/commits/5e2514d))
* Added ErrorMessagesComponent, added specs ([c0c0b95](https://bitbucket.org/DcsMarcRemolt/dcs-angular2-utils/commits/c0c0b95))



<a name="0.0.2"></a>
## 0.0.2 (2016-12-09)


### Features

* Added conventional-changelog-lint ([5e2514d](https://bitbucket.org/DcsMarcRemolt/dcs-angular2-utils/commits/5e2514d))
* Added ErrorMessagesComponent, added specs ([c0c0b95](https://bitbucket.org/DcsMarcRemolt/dcs-angular2-utils/commits/c0c0b95))



<a name="0.0.1"></a>
## 0.0.1 (2016-12-07)



